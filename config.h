/* user and group to drop privileges to */
static const char *user  = "vmaillot";
static const char *group = "users";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "black",     /* after initialization */
	[INPUT] =  "#606060",   /* during input */
	[FAILED] = "white",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
